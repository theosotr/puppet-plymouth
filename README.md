# plymouth

## Overview

This module makes it easy to manage plymouth on client computers running
Linux.

## Module Description

There are two main functions to this module:

* choose whether plymouth should be ran or not
* choose what plymouth theme will be used

If plymouth installation is set to `true`, it will automatically configure 
GRUB to use plymouth.

## Setup

### What plymouth affects

This module starts by installing these two packages:

* `plymouth`
* `plymouth-themes`

It then replaces the `GRUB_CMDLINE_LINUX_DEFAULT="quiet"` line in
`/etc/default/grub` with the default `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"`
in order to add the 'splash' option in the default GRUB boot.

For this change to be effective, the command `update-grub2` is ran.

If you instead wish to pass custom parameter to `GRUB_CMDLINE_LINUX_DEFAULT`
you can use the `grub_override` parameter.

This module also sets the client's default plymouth theme to the chosen 
theme. By default the theme `spinfinity` is used if no theme is specified. 
This is done by running the `plymouth-set-default-theme` command.

Finally the new initramfs image is created by running the `update-initramfs` 
command.

## Usage

**This modules depends on [stdlib][] to run.**

This module has three parameters:

* `ensure`
* `theme`
* `grub_override`

[stdlib]: https://github.com/puppetlabs/puppetlabs-stdlib

You can use this module by calling its class. A more advanced usage would look
like:

    class { 'plymouth':
      theme         => 'solar',
      grub_override => 'quiet splash nomodeset';
    }

### ensure

Data type: Enum

The `ensure` parameter enables or disables plymouth on client computers. It
accepts two values, `present` and `absent`.

If the parameter is set from `present` to `absent`, plymouth and related
packages will be uninstalled and the "splash" option will simply be disabled in
GRUB.

Default value: `present`

### theme

Data type: String

The `theme` parameter lets you choose what theme plymouth will use. The
following themes are available by default:

* fade-in: features a centered logo that fades in and out while stars twinkle
   around the logo during system boot up.
* glow: features a pie chart as progress indicator.
* script: features a simple base theme.
* solar: features a blue flamed sun with animated solar flares.
* spinfinity: features a centered logo and animated spinner that spins in the
   shape of an infinity sign.
* spinner: features a simple theme with a small spinner on a dark background.

Default value: `spinfinity`

### grub_override

Data type: String

Sometimes you need to pass other parameters to grub than just `quiet splash`.
This is especially true if you still use old monitors and need to set `nomodeset`.

To do this use the `grub_override` parameter. Anything given to that parameter
will be outputted in `GRUB_CMDLINE_LINUX_DEFAULT=`.

Default value: `undef`

## Limitations

This module has been developed for clients using Debian Stretch and Puppet 4.x.

## Development

If you  want to contribute to this module, please send a pull request. If you
find a bug, please submit a bug issue in the tracker.
