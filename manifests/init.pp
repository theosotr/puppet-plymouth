class plymouth (
  Enum[present, absent] $ensure = present,
  String $theme = 'spinfinity',
  Variant[String, Undef] $grub_override = undef,
) {

  exec { 'update-grub2':
    command     => '/usr/sbin/update-grub2',
    refreshonly => true;
  }

  if $plymouth::ensure == 'present' {
    include ::plymouth::present
  }
  else {
    include ::plymouth::absent
  }
}
