class plymouth::present {

  exec {
    'plymouth-set-default-theme':
      command => "/usr/sbin/plymouth-set-default-theme -R ${plymouth::theme}",
      unless  => "/usr/sbin/plymouth-set-default-theme |grep ${plymouth::theme}",
      require => Package['plymouth-themes'];
  }

  package { ['plymouth', 'plymouth-themes']:
    ensure => present;
  }

  if $plymouth::grub_override {

    file_line { 'enable grub splash':
      ensure  => present,
      path    => '/etc/default/grub',
      line    => "GRUB_CMDLINE_LINUX_DEFAULT='${plymouth::grub_override}'",
      match   => '^GRUB_CMDLINE_LINUX_DEFAULT',
      notify  => Exec['update-grub2'],
      require => Package['plymouth'];
    }
  }

  else {

    file_line { 'enable grub splash':
      ensure  => present,
      path    => '/etc/default/grub',
      line    => 'GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"',
      match   => '^GRUB_CMDLINE_LINUX_DEFAULT',
      notify  => Exec['update-grub2'],
      require => Package['plymouth'];
    }
  }
}
